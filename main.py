import pygame
import os
from pygame import *

pygame.init()

gameWindow = pygame.display.set_mode((1200,800))

pygame.display.set_caption("Alpha 0.01")

clock = pygame.time.Clock()



walkRight = pygame.image.load('/home/pineapple/PycharmProjects/pygame1/images/maincharacter/rightmain.png')
walkLeft = pygame.image.load('/home/pineapple/PycharmProjects/pygame1/images/maincharacter/leftmain.png')
walkDown = pygame.image.load('/home/pineapple/PycharmProjects/pygame1/images/maincharacter/frontmain.png')
walkUp = pygame.image.load('/home/pineapple/PycharmProjects/pygame1/images/maincharacter/backmain.png')

bg = pygame.image.load('/home/pineapple/PycharmProjects/pygame1/images/test2.png')

walkRight = transform.scale(walkRight,(32,32))
walkLeft = transform.scale(walkLeft,(32,32))
walkDown = transform.scale(walkDown,(32,32))
walkUp = transform.scale(walkUp,(32,32))
bg = transform.scale(bg,(2000,2000))



windowWidth, windowHeight = pygame.display.get_surface().get_size()
windowWidth = windowWidth / 2
windowHeight = windowHeight / 2

class tree(object):

    def __init__(self,x,y,width,height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

class player(object):
    def __init__(self,x,y,width,height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 5
        self.left = False
        self.right = False
        self.down = False
        self.up = True



    def draw(self):
        keys = pygame.key.get_pressed()

        if keys[pygame.K_LEFT]:
            self.x -= self.vel
            self.left = True
            self.right = False
            self.down = False
            self.up = False

        if keys[pygame.K_RIGHT]:
            self.x += self.vel
            self.left = False
            self.right = True
            self.down = False
            self.up = False
        if keys[pygame.K_UP]:
            self.y -= self.vel
            self.left = False
            self.right = False
            self.down = False
            self.up = True
        if keys[pygame.K_DOWN]:
            self.y += self.vel
            self.left = False
            self.right = False
            self.down = True
            self.up = False







def redrawGameWindow():



    if character.left:
        gameWindow.blit(walkLeft,(windowWidth,windowHeight))
    if character.right:
        gameWindow.blit(walkRight,(windowWidth,windowHeight))
    if character.down:
        gameWindow.blit(walkDown,(windowWidth,windowHeight))
    if character.up:
        gameWindow.blit(walkUp,(windowWidth,windowHeight))
    pygame.display.update()



character = player(50,50,32,32)
running = True
while running:
    gameWindow.blit(bg, (-character.x, -character.y))
    redrawGameWindow()

    clock.tick()

    print(clock.get_fps())
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    character.draw()

pygame.quit()